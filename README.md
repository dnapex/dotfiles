# My Personal Dotfiles
This repository contains personal configuration files for programs that I use frequently. Feel free to clone this repository for your own use.

## Dependencies
Some of the configurations require additional dependencies. In most cases, this is due to software specific plugins.

### Vimrc
The neocomplete plugin which I use requires Vim 7.3.885+ with Lua enabled. This can be installed as follows:

#### OSX
Using homebrew:
```
brew install vim --with-lua
```

#### Linux
Install the `vim-nox` package:
```
# For Debian based systems
apt-get install vim-nox

# For Arch Linux
yaourt install vim-nox
```

#### Others
Please refer to the recommendations at: https://github.com/Shougo/neocomplete.vim 

## Installation
Just run the included installation script to install my dotfiles.

```
./install.sh
```
